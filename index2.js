const {Builder, Browser, By, Key, until} = require('selenium-webdriver');
const fs = require('fs');
const path = require('path');
const Chrome = require("selenium-webdriver/chrome");

(async function example() {
    const options = new Chrome.Options();
    options.addArguments('user-data-dir=' + path.resolve("user-data"))
    const driver = await new Builder().forBrowser(Browser.CHROME).setChromeOptions(options).build();
    await driver.get('https://www.clickifycloud.com/clientarea.php?action=services');
    await driver.wait(until.titleIs("Client Area - Clickify Cloud"));
    await sleep(30000);

    await driver.quit();
})();

var sleep= function(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
